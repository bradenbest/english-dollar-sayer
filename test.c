#include <stdio.h>
#include <string.h>

#include "dollar.h"
#include "util.h"

int
check_test(int expr)
{/*{{{*/
    puts(
            expr
            ? "    \e[32mPASS\e[0m"
            : "    \e[31mFAIL\e[0m");

    return !expr;
}/*}}}*/

int
test_chinstr()
{/*{{{*/
    int failed = 0;
    char *str;
    char *result;

    printf("testing chinstr\n");
    printf("  for '.' in \"Hello World\" (expect: NULL)\n");

    str = "Hello World";
    result = chinstr('.', str);
    printf("    %s\n", result);
    failed += check_test(result == NULL);

    printf("  for '.' in \"Hello.World\" (expect: \".World\")\n");

    str = "Hello.World";
    result = chinstr('.', str);
    printf("    %s\n", result);
    failed += check_test(!strncmp(result, ".World", 6));

    return failed;
}/*}}}*/

int
test_pad_3()
{/*{{{*/
    int failed = 0;
    char str[50];
    char *result;

    printf("testing pad_3\n");
    printf("  for length = 5 (expect: 123450)\n");

    memset(str, 0, 50);
    memcpy(str, "12345", 5);
    result = pad_3(str);
    printf("    %s\n", result);
    failed += check_test(!strncmp(result, "123450", 6));

    printf("  for length = 4 (expect: 123400)\n");

    memset(str, 0, 50);
    memcpy(str, "1234", 5);
    result = pad_3(str);
    printf("    %s\n", result);
    failed += check_test(!strncmp(result, "123400", 6));

    return failed;
}/*}}}*/

int
test_reverse()
{/*{{{*/
    int failed = 0;
    char *result;

    printf("testing reverse\n");
    printf("  for \"1578\" (expect: \"875100\")\n");

    result = reverse("1578");
    printf("    %s\n", result);
    failed += check_test(!strncmp(result, "875100", 6));

    printf("  for \"1578.98\" (expect: \"875100\")\n");

    result = reverse("1578.98");
    printf("    %s\n", result);
    failed += check_test(!strncmp(result, "875100", 5));

    return failed;
}/*}}}*/

int
test_strlenx()
{/*{{{*/
    int failed = 0;
    size_t result;

    printf("testing strlenx (terminator: '.')\n");
    printf("  for \"Hello World\" (expect: 11)\n");

    result = strlenx("Hello World", ".");
    printf("    %lu\n", result);
    failed += check_test(result == 11);

    printf("  for \"Hello.World\" (expect: 5)\n");

    result = strlenx("Hello.World", ".");
    printf("    %lu\n", result);
    failed += check_test(result == 5);

    return failed;
}/*}}}*/

int
test_parse()
{/*{{{*/
    int failed = 0;
    char *input;
    struct group result[6];

    printf("testing parse\n");
    printf("  for \"12345.12\" (expect: { {1, -1, 120}, {1, 0, 543}, {1, 1, 210}, {0, 0, 000} (3 times) })\n");

    input = "12345.12";
    memset(result, 0, sizeof(struct group) * 6);
    parse(result, input);
    printf("    [0]: {%i, %i, \"%c%c%c\"}\n",
            result[0].defined,
            result[0].magnitude,
            result[0].digits[0],
            result[0].digits[1],
            result[0].digits[2]);
    failed += check_test(result[0].defined
            && result[0].magnitude == -1
            && result[0].digits[0] == '1'
            && result[0].digits[1] == '2');
    printf("    [1]: {%i, %i, \"%c%c%c\"}\n",
            result[1].defined,
            result[1].magnitude,
            result[1].digits[0],
            result[1].digits[1],
            result[1].digits[2]);
    failed += check_test(result[1].defined
            && result[1].magnitude == 0
            && result[1].digits[0] == '5'
            && result[1].digits[1] == '4'
            && result[1].digits[2] == '3');
    printf("    [2]: {%i, %i, \"%c%c%c\"}\n",
            result[2].defined,
            result[2].magnitude,
            result[2].digits[0],
            result[2].digits[1],
            result[2].digits[2]);
    failed += check_test(result[2].defined
            && result[2].magnitude == 1
            && result[2].digits[0] == '2'
            && result[2].digits[1] == '1'
            && result[2].digits[2] == '0');
    printf("    [3]: {%i, %i, \"%c%c%c\"}\n",
            result[3].defined,
            result[3].magnitude,
            result[3].digits[0],
            result[3].digits[1],
            result[3].digits[2]);
    failed += check_test(!result[3].defined
            && result[3].magnitude == 0
            && result[3].digits[0] == 0
            && result[3].digits[1] == 0
            && result[3].digits[2] == 0);

    printf("  for \"12345\" (expect: { {1, 0, 543}, {1, 1, 210}, {0, 0, 000} (4 times) })\n");

    input = "12345";
    memset(result, 0, sizeof(struct group) * 6);
    parse(result, input);
    printf("    [0]: {%i, %i, \"%c%c%c\"}\n",
            result[0].defined,
            result[0].magnitude,
            result[0].digits[0],
            result[0].digits[1],
            result[0].digits[2]);
    failed += check_test(result[0].defined
            && result[0].magnitude == 0
            && result[0].digits[0] == '5'
            && result[0].digits[1] == '4'
            && result[0].digits[2] == '3');
    printf("    [1]: {%i, %i, \"%c%c%c\"}\n",
            result[1].defined,
            result[1].magnitude,
            result[1].digits[0],
            result[1].digits[1],
            result[1].digits[2]);
    failed += check_test(result[1].defined
            && result[1].magnitude == 1
            && result[1].digits[0] == '2'
            && result[1].digits[1] == '1'
            && result[1].digits[2] == '0');
    printf("    [2]: {%i, %i, \"%c%c%c\"}\n",
            result[2].defined,
            result[2].magnitude,
            result[2].digits[0],
            result[2].digits[1],
            result[2].digits[2]);
    failed += check_test(!result[2].defined
            && result[2].magnitude == 0
            && result[2].digits[0] == 0
            && result[2].digits[1] == 0
            && result[2].digits[2] == 0);

    return failed;
}/*}}}*/

int
test()
{/*{{{*/
    int failed = 0;
    int total = 2 + 2 + 2 + 2 + 7;

    failed += test_chinstr();
    failed += test_pad_3();
    failed += test_strlenx();
    failed += test_reverse();
    failed += test_parse();

    printf("Unit tests complete. %i failed out of %i total\n", failed, total);

    return failed;
}/*}}}*/

int
main()
{/*{{{*/
    return test();
}/*}}}*/
