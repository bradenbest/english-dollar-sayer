#ifndef UTIL_H
#define UTIL_H

void die(char *msg);
char *chinstr(char ch, char *str);
char *chinmem(char ch, char *mem, size_t n);
char *pad_3(char *buf);
size_t strlenx(char *str, char *terminators);
char *strdup(char *src);

#endif
