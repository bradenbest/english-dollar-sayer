#!/bin/bash

dollar(){
    ../dollar -dm ../dict/magnitude/trillion.dict -dc ../dict/currencies/${1}.dict $2
}

main(){
    to=$1
    amount=$2
    result=$(./convert.py $to $amount)

    echo "usd:"
    dollar usd $amount

    echo "${to}:"
    dollar $to $result
}

main $@
