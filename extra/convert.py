#!/usr/bin/env python

import sys

conv = {
    "usd":  1.00,
    "gbp":  0.75,
    "euro": 0.89
}

def convert(to, amount):
    return float(amount) * conv[to]

def print_convert(to, amount):
    result = convert(to, amount)
    print "%0.2f" % result

def usage():
    print "convert.py - convert USD to a different currency\n"
    print "usage: convert.py [to] [amount]"
    print "to:"
    print "  usd   US Dollar (redundant)"
    print "  gbp   Great British Pound"
    print "  euro  Euros"
    print "amount:"
    print "  Any number"
    print "warning: you should avoid using this script for converting huge numbers, as the floating point precision is limited"

def main():
    if len(sys.argv) == 3:
        print_convert(sys.argv[1], sys.argv[2])
    else:
        usage()

main()
