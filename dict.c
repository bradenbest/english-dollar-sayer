#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dict.h"
#include "file.h"
#include "util.h"

static char *dict_num[] = {
    "zero", "one",    "two",    "three",    "four",     "five",    "six",     "seven",     "eight",    "nine",
    "ten",  "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
    "",     "",       "twenty", "thirty",   "forty",    "fifty",   "sixty",   "seventy",   "eighty",   "ninety"
};

static char *dict_currency[] = {
    "dollar", "dollars", "cent", "cents"
};

static char *dict_magnitude[] = {
    "",            "thousand",    "million",     "billion",      "trillion",
    "quadrillion", "quintillion", "sexillion",   "septillion",   "octillion",
    "nonillion",   "decillion",   "undecillion", "duodecillion", "tredecillion"
};

struct dict dict = {
    .num = dict_num,
    .currency = dict_currency,
    .currency_length = 4,
    .magnitude = dict_magnitude,
    .magnitude_length = 15
};

static void dict_free_currency();
static void dict_free_magnitude();

static void
dict_free_currency()
{/*{{{*/
    for(int i = 0; i < dict.currency_length; i++)
        free(dict.currency[i]);

    free(dict.currency);
    dict.currency_length = 0;
    dict.flag_currency_set = 0;
}/*}}}*/

static void
dict_free_magnitude()
{/*{{{*/
    for(int i = 0; i < dict.magnitude_length; i++)
        free(dict.magnitude[i]);

    free(dict.magnitude);
    dict.magnitude_length = 0;
    dict.flag_magnitude_set = 0;
}/*}}}*/

void
dict_set_currency(char *filename)
{/*{{{*/
    FILE *f = fopen_safe(filename, "r");

    if(dict.flag_currency_set)
        die("Please don't spam -dc/-dm\n");

    dict.currency_length = file_get_length(f);
    dict.currency = calloc(sizeof (char *), dict.currency_length);
    file_parse_list(dict.currency, f);
    dict.flag_currency_set = 1;
    fclose(f);
}/*}}}*/

void
dict_set_magnitude(char *filename)
{/*{{{*/
    FILE *f = fopen_safe(filename, "r");

    if(dict.flag_magnitude_set)
        die("Please don't spam -dc/-dm\n");

    dict.magnitude_length = file_get_length(f);
    dict.magnitude = calloc(sizeof (char *), dict.magnitude_length);
    file_parse_list(dict.magnitude, f);
    dict.flag_magnitude_set = 1;
    fclose(f);
}/*}}}*/

void
dict_free()
{/*{{{*/
    if(dict.flag_magnitude_set)
        dict_free_magnitude();

    if(dict.flag_currency_set)
        dict_free_currency();
}/*}}}*/
