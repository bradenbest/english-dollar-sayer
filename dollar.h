#ifndef DOLLAR_H
#define DOLLAR_H
#include <stddef.h>

struct group {
    int defined;
    int magnitude;
    char digits[3];
};

char *reverse(char *src);
void parse(struct group *list, char *src);
void parse_group_array(char *buf, struct group *list);
char *to_english(char *src);

#endif
