#include <string.h>
#include <stdlib.h>

#include "dollar.h"
#include "write.h"
#include "util.h"
#include "dict.h"

char *
reverse(char *src)
{/*{{{*/
    static char buf[50];
    char *bufp = buf;
    int i;

    memset(buf, 0, 50);

    for(i = strlenx(src, ".") - 1; i >= 0; i--)
        *(bufp++) = src[i];

    return pad_3(buf);
}/*}}}*/

void
parse(struct group *list, char *src)
{/*{{{*/
    char *decimal = chinstr('.', src);
    char *src_rev = reverse(src);
    int magnitude = 0;

    if(decimal){
        memcpy(list->digits, decimal + 1, 2); // {ten, hundred, 0} -- {5,9,0} = 0.59
        list->magnitude = -1;
        list->defined = 1;
        list++;
    }

    while(*src_rev){
        memcpy(list->digits, src_rev, 3); // {one, ten, hundred} -- {1,5,9} = 951
        list->magnitude = magnitude++;
        list->defined = 1;
        list++;
        src_rev += 3;
    }
}/*}}}*/

void
parse_group_array(char *buf, struct group *list)
{/*{{{*/
    struct group *base = list;

    if(!list->defined)
        return;

    while(list[1].defined)
        list++;

    while(list - base >= 0)
        write_chunk(buf, list--);
}/*}}}*/

char *
to_english(char *src)
{/*{{{*/
    size_t adjusted_length = dict.magnitude_length + 2;
    char *buf = calloc(sizeof (char), adjusted_length * 100);
    struct group *groups = calloc(sizeof (struct group), adjusted_length);

    parse(groups, src);
    parse_group_array(buf, groups);
    free(groups);

    return buf;
}/*}}}*/
