#ifndef FILE_H
#define FILE_H

int file_get_length(FILE *f);
void file_parse_list(char **dict_p, FILE *f);
char *freadtil(FILE *f, char *terminators);
FILE *fopen_safe(char *filename, char *mode);

#endif

