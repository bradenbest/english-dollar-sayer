# English Dollar...sayer...thing...

Based on a challenge. Here's the description:

> make a program that converts digits to words
>
> in currency
>
> eg. $100.99 to hundred dollars ninety-nine cents

I asked him how high he wanted it to go, and he said

> Billion will be (A-OK emoji)

So I took it up to eleven and wrote it to interpret input all the way up to 999 tredecillion

## Build

To build it, just run `make`

    $ make

This will build the program and run the unit tests. From there, you can run the program, `./dollar`
