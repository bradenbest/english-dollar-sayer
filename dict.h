#ifndef DICT_H
#define DICT_H

struct dict {
    char **num;

    char **currency;
    int currency_length;
    int flag_currency_set;

    char **magnitude;
    int magnitude_length;
    int flag_magnitude_set;
};

extern struct dict dict;

void dict_set_magnitude(char *filename);
void dict_set_currency(char *filename);
void dict_free();

#endif
