#include <stdio.h>
#include <stdlib.h>

#include "file.h"
#include "util.h"

int
file_get_length(FILE *f)
{/*{{{*/
    char *contents = freadtil(f, ".");
    int ret;

    if(!contents)
        die("Invalid file format\n");

    ret = atoi(contents);
    free(contents);

    return ret;
}/*}}}*/

void
file_parse_list(char **dict_p, FILE *f)
{/*{{{*/
    char *contents;

    while((contents = freadtil(f, ",;")) != NULL)
        *(dict_p++) = contents;
}/*}}}*/

char *
freadtil(FILE *f, char *terminators)
{/*{{{*/
    int c;
    char buf[1000] = "";
    char *buf_p = buf;

    while((c = fgetc(f)) != EOF && buf_p - buf < 1000){
        if(chinmem(c, terminators, strlenx(terminators, "")))
            return strdup(buf);

        *(buf_p++) = c;
    }

    return NULL;
}/*}}}*/

FILE *
fopen_safe(char *filename, char *mode)
{/*{{{*/
    FILE *f = fopen(filename, mode);

    if(!f){
        fprintf(stderr, "File given: %s\n", filename);
        die("Couldn't open file\n");
    }

    return f;
}/*}}}*/
