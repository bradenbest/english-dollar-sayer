CFLAGS = -Wall -pedantic -std=c99

all: dollar test

dollar: main.o dollar.o write.o util.o args.o dict.o file.o
	$(CC) $^ -o $@

test: test.o dollar.o write.o util.o dict.o file.o
	$(CC) $^ -o $@
	./test

%.o: %.c
	$(CC) $(CFLAGS) $^ -c

clean:
	rm -f *.o dollar test

.PHONY: clean all
