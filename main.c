#include "args.h"
#include "dict.h"

int
main(int argc, char **argv)
{/*{{{*/
    parse_args(argv + 1);
    dict_free();

    return 0;
}/*}}}*/
