#include <string.h>

#include "dollar.h"
#include "write.h"
#include "dict.h"

static void write_space(char *buf);
static void write_ones(char *buf, int digit);
static void write_tens(char *buf, int digit);
static void write_hundreds(char *buf, int digit);
static void write_group_name(char *buf, int magnitude);
static void write_digits_2(char *buf, int *digits);
static int write_digits_3(char *buf, int *digits);
static int digits_2_to_number(int *digits);
static int digits_3_to_number(int *digits);
static void write_decimal_group(char *buf, int *digits);
static void write_normal_group(char *buf, int *digits, int magnitude);
static void write_zero_group(char *buf, int *digits, struct group *next);
static void convert_digits_2(int *dest, char *src);
static void convert_digits_3(int *dest, char *src);

static void
write_space(char *buf)
{/*{{{*/
    strcat(buf, " ");
}/*}}}*/

static void
write_ones(char *buf, int digit)
{/*{{{*/
    strcat(buf, dict.num[digit]);
}/*}}}*/

static void
write_tens(char *buf, int digit)
{/*{{{*/
    strcat(buf, dict.num[20 + digit]);
}/*}}}*/

static void
write_hundreds(char *buf, int digit)
{/*{{{*/
    strcat(buf, dict.num[digit]);
    write_space(buf);
    strcat(buf, "hundred");
}/*}}}*/

static void
write_group_name(char *buf, int magnitude)
{/*{{{*/
    strcat(buf, dict.magnitude[magnitude]);
}/*}}}*/

static void
write_digits_2(char *buf, int *digits)
{/*{{{*/
    enum { ten, one };

    if(digits[ten] < 2)
        write_ones(buf, digits_2_to_number(digits));

    else {
        write_tens(buf, digits[ten]);

        if(digits[one]){
            strcat(buf, "-");
            write_ones(buf, digits[one]);
        }
    }
}/*}}}*/

static int
write_digits_3(char *buf, int *digits)
{/*{{{*/
    enum { hun, ten, one };
    int write_2 = digits[ten] || digits[one],
        write_3 = digits[hun];

    if(write_3) {
        write_hundreds(buf, digits[hun]);

        if(write_2)
            strcat(buf, " and ");
    }

    if(write_2)
        write_digits_2(buf, digits + 1);

    return write_2 || write_3;
}/*}}}*/

static int
digits_2_to_number(int *digits)
{/*{{{*/
    enum { ten, one };

    return digits[ten] * 10 + digits[one];
}/*}}}*/

static int
digits_3_to_number(int *digits)
{/*{{{*/
    enum { hun, ten, one };

    return digits[hun] * 100 + digits[ten] * 10 + digits[one];
}/*}}}*/

static void
write_decimal_group(char *buf, int *digits)
{/*{{{*/
    int use_plural = digits_2_to_number(digits) != 1;

    strcat(buf, "and ");
    write_digits_2(buf, digits);
    write_space(buf);
    strcat(buf, dict.currency[2 + use_plural]);
}/*}}}*/

static void
write_normal_group(char *buf, int *digits, int magnitude)
{/*{{{*/
    if(write_digits_3(buf, digits)){
        write_space(buf);
        write_group_name(buf, magnitude);
        write_space(buf);
    }
}/*}}}*/

static void
write_zero_group(char *buf, int *digits, struct group *next)
{/*{{{*/
    enum { hun, ten, one };
    int is_biggest_group = !next->defined,
        use_plural = !is_biggest_group || digits_3_to_number(digits) != 1;

    if(write_digits_3(buf, digits))
        write_space(buf);

    else if(is_biggest_group){
        write_ones(buf, 0);
        write_space(buf);
    }

    strcat(buf, dict.currency[use_plural]);
    write_space(buf);
}/*}}}*/

static void
convert_digits_2(int *dest, char *src)
{/*{{{*/
    for(int i = 0; i < 2; i++)
        dest[i] = src[i] - '0';
}/*}}}*/

static void
convert_digits_3(int *dest, char *src)
{/*{{{*/
    for(int i = 0; i < 3; i++)
        dest[i] = src[2 - i] - '0';
}/*}}}*/

void
write_chunk(char *buf, struct group *g)
{/*{{{*/
    int digits[3];

    switch(g->magnitude){
        case -1:
            convert_digits_2(digits, g->digits);
            write_decimal_group(buf, digits);
            break;

        case 0:
            convert_digits_3(digits, g->digits);
            write_zero_group(buf, digits, g + 1);
            break;

        default:
            convert_digits_3(digits, g->digits);
            write_normal_group(buf, digits, g->magnitude);
            break;
    }
}/*}}}*/
