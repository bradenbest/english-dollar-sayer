#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

void
die(char *msg)
{/*{{{*/
    fputs(msg, stderr);
    exit(1);
}/*}}}*/

char *
chinstr(char ch, char *str)
{/*{{{*/
    for(; *str; str++)
        if(ch == *str)
            return str;

    return NULL;
}/*}}}*/

char *
chinmem(char ch, char *mem, size_t n)
{/*{{{*/
    for(int i = 0; i < n; i++)
        if(ch == mem[i])
            return mem + i;

    return NULL;
}/*}}}*/

char *
pad_3(char *buf)
{/*{{{*/
    while(strlen(buf) % 3)
        strcat(buf, "0");

    return buf;
}/*}}}*/

size_t
strlenx(char *str, char *terminators)
{/*{{{*/
    char *base = str;

    while(*str != '\0' && !chinstr(*str, terminators))
        str++;

    return str - base;
}/*}}}*/

char *
strdup(char *src)
{/*{{{*/
    char *out = calloc(sizeof (char), strlen(src) + 1);

    if(out)
        strcpy(out, src);

    return out;
}/*}}}*/
