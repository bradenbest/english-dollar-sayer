#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "dollar.h"
#include "util.h"
#include "dict.h"
#include "version.h"

enum validate_error {
    VE_OK,
    VE_ILLEGAL,
    VE_TOOSHORT,
    VE_TOOLONG,
    VE_TOOMANYDOTS,
    VE_STRINGTOOLONG,
    VE_LEADINGZERO,
    VE_NOINTEGER,
    VE_END
};

static const char *validate_error_str[VE_END] = {
    [VE_OK]            = "OK",
    [VE_ILLEGAL]       = "Illegal character (must be [0-9] or '.')",
    [VE_TOOSHORT]      = "Decimal portion is too short (see Usage::Format)",
    [VE_TOOLONG]       = "Decimal portion is too long (see Usage::Format)",
    [VE_TOOMANYDOTS]   = "Too many dots",
    [VE_STRINGTOOLONG] = "The string is too long. I don't have words for numbers that big (see Usage::Maximum (default))",
    [VE_LEADINGZERO]   = "Erroneous leading 0.",
    [VE_NOINTEGER]     = "Missing integer portion ('.XX' is not allowed, see Usage::Format)"
};

static int validate_charset(char *src);
static enum validate_error validate(char *src);
static void usage();
static void parse_opt_dict(char *arg, char *param);
static void parse_opt(char *arg, char *param);
static void parse_arg(char *arg);

static int
validate_charset(char *src)
{/*{{{*/
    for(; *src; src++)
        if(chinstr(*src, "0123456789.") == NULL)
            return 0;

    return 1;
}/*}}}*/

static enum validate_error
validate(char *src)
{/*{{{*/
    char *dot_ptr = chinstr('.', src);

    if(!validate_charset(src)) // ex. 1,000; -1; gibberish
        return VE_ILLEGAL;

    if(dot_ptr++){
        if(chinmem('\0', dot_ptr, 2)) // ex. 1.0
            return VE_TOOSHORT;

        if(dot_ptr[2]) // ex. 1.000
            return VE_TOOLONG;

        if(chinstr('.', dot_ptr)) // ex. 1...
            return VE_TOOMANYDOTS;
    }

    if(strlenx(src, ".") > 3 * dict.magnitude_length) // ex. 1000000000000000000000000000000000000000000000 
        return VE_STRINGTOOLONG;

    if(src[0] == '0' && !chinmem(src[1], ".\0", 2)) // ex. 01
        return VE_LEADINGZERO;

    if(src[0] == '.') // ex. .01
        return VE_NOINTEGER;

    return VE_OK;
}/*}}}*/

static void
usage()
{/*{{{*/
    die( PROGNAME " " VERSION " - convert a numeric dollar amount to english\n\n"
         "Usage: " PROGNAME " [options] number1 [number2 ...]\n"
         "Options:\n"
         "  -dc [file]  set [file] as the currency dictionary (see usd.dict for an example)\n"
         "  -dm [file]  set [file] as the magnitude dictionary (see default.dict for an example)\n"
         "\n"
         "Format: X[.YY] (ex. 5.26, 7, 146, 150.00)\n"
         "Maximum: 9.99... * 10^44 (hundred tredecillion)\n");
}/*}}}*/

static void
parse_opt_dict(char *arg, char *param)
{/*{{{*/
    switch(*arg){
        case 'm':
            dict_set_magnitude(param);
            break;

        case 'c':
            dict_set_currency(param);
            break;

        default:
            break;
    }
}/*}}}*/

static void
parse_opt(char *arg, char *param)
{/*{{{*/
    switch(*arg){
        case 'd':
            parse_opt_dict(arg + 1, param);
            break;

        default:
            break;
    }
}/*}}}*/

static void
parse_arg(char *arg)
{/*{{{*/
    enum validate_error vresult = validate(arg);
    char *engresult = NULL;

    printf("%s: ", arg);

    if(vresult == VE_OK)
        printf("%s\n", (engresult = to_english(arg), engresult));
    else
        printf("E%03u - %s\n", vresult, validate_error_str[vresult]);

    free(engresult);
}/*}}}*/

void
parse_args(char **argv)
{/*{{{*/
    if(*argv == NULL)
        usage();

    while(*argv){
        if(**argv == '-'){
            parse_opt((*argv) + 1, *(argv + 1));
            argv++;
        }

        else
            parse_arg(*argv);

        argv++;
    }
}/*}}}*/
